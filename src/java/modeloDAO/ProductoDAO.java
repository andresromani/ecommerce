package modeloDAO;

import config.DB;
import interfaces.CRUD;
import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import modelo.Producto;

public class ProductoDAO implements CRUD {

    DB cnx = new DB();
    Connection con = null;
    PreparedStatement ps;
    ResultSet rs;
    
    @Override
    public List listar() {
        ArrayList<Producto> list = new ArrayList<>();
        String constulaSQL = "SELECT * FROM productos";
        
        try {
            con = cnx.getConnection();
            ps = con.prepareStatement(constulaSQL);
            rs = ps.executeQuery();
            
            while (rs.next()) {
                Producto prod = new Producto();
                prod.setId(rs.getInt("id"));
                prod.setNombre(rs.getString("nombre"));
                prod.setDescripcion(rs.getString("descripcion"));
                prod.setPrecio(rs.getDouble("precio"));
                prod.setCategoria(rs.getInt("id_categoria"));
                prod.setImagen(rs.getString("imagen"));
                list.add(prod);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                ps.close();
                rs.close();
                con.close();
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
        
        return list;
    }

    @Override
    public boolean add(Producto prod) {
        String sql = "INSERT INTO productos VALUES ("+prod.getId()+", '"+prod.getNombre()+"', '"+prod.getDescripcion()+"', "+prod.getPrecio()+", "+prod.getCategoria()+", '"+prod.getImagen()+"')";
        
        try {
            con = cnx.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                ps.close();
                con.close();
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
        
        return false;
    }
    
}
