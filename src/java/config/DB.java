package config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Iterator;
import java.util.List;
import modelo.Producto;
import modeloDAO.ProductoDAO;

public class DB {
    
    Connection con;
    String host = "jdbc:mysql://localhost:3306/ecommerce?useTimezone=true&serverTimezone=UTC";
    String user = "empleado";
    String password = "empleado";
    Producto prod = new Producto();
    
    public DB() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection(host, user, password);
            System.out.println("Conexión exitosa!!!");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    public Connection getConnection() {
        return con;
    }
    
    public int generarId() {
        ProductoDAO dao = new ProductoDAO();
        List list = dao.listar();
        Iterator<Producto> iter = list.iterator();
        
        while (iter.hasNext()) {
            prod = iter.next();
        }
        
        return prod.getId() + 1;
    }
    
    public static void main (String[]args) {
        
        DB cnx = new DB();
        
        Connection con = cnx.getConnection();
        
    }
    
}
