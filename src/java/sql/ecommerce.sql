-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: ecommerce
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categorias`
--

DROP TABLE IF EXISTS `categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categorias` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorias`
--

LOCK TABLES `categorias` WRITE;
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT INTO `categorias` VALUES (1,'TVs'),(2,'Monitores'),(3,'Notebooks'),(4,'Consolas'),(5,'Muebles'),(6,'Electrodomésticos');
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `productos` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `precio` decimal(18,2) NOT NULL,
  `id_categoria` int NOT NULL,
  `imagen` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_prodcat` (`id_categoria`),
  CONSTRAINT `fk_prodcat` FOREIGN KEY (`id_categoria`) REFERENCES `categorias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES (1,'Smart TV 32\" Full HD','Aca va la descripción',17439.99,1,'http://s2.subirimagenes.com/otros/previo/thump_9927707smarttv32.jpg'),(2,'Smart TV 43\" 4k','Aca va la descripción',30000.00,1,'http://s2.subirimagenes.com/otros/previo/thump_9927708smarttv43.jpg'),(3,'Smart TV 55\" 4k','Aca va la descripción',61500.00,1,'http://s2.subirimagenes.com/otros/previo/thump_9927709smarttv55.jpg'),(4,'Monitor 19\" HD','Aca va la descripción',9000.00,2,'http://s2.subirimagenes.com/otros/previo/thump_9927711monitor19.jpg'),(5,'Monitor 22\" Full HD','Aca va la descripción',13000.00,2,'http://s2.subirimagenes.com/otros/previo/thump_9927712monitor22.jpg'),(6,'Monitor 24\" Full HD Curvo','Aca va la descripción',30999.00,2,'http://s2.subirimagenes.com/otros/previo/thump_9927713monitor24.jpg'),(7,'Notebook i3','Aca va la descripción',70000.00,3,'http://s2.subirimagenes.com/otros/previo/thump_9927716notebooki3.jpg'),(8,'Notebook i5','Aca va la descripción',149999.00,3,'http://s2.subirimagenes.com/otros/previo/thump_9927715notebooki5.jpg'),(9,'Notebook i7','Aca va la descripción',199999.99,3,'http://s2.subirimagenes.com/otros/previo/thump_9927717notebooki7.jpg'),(10,'Escritorio PC','Aca va la descripción',9000.00,5,'http://s2.subirimagenes.com/otros/previo/thump_9927718escritoriopc.jpg'),(12,'Xbox One','Acá va la descripción',55000.00,4,'http://s2.subirimagenes.com/otros/previo/thump_9928175dnqnp677402mla327318.jpg'),(13,'Play Station 4','Acá va la descripción',55000.00,4,'http://s2.subirimagenes.com/imagen/previo/thump_9927735ps4pro.png'),(14,'Ventilador Pared','Acá va la descripción',7199.00,6,'http://s2.subirimagenes.com/otros/previo/thump_9928194ventiladorpared.jpg');
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-13 18:29:09
