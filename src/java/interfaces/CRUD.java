package interfaces;

import java.util.List;
import modelo.Producto;

public interface CRUD {
    
    public List listar();
    public boolean add(Producto prod);
    
}