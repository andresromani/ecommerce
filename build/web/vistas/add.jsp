<%@page import="config.DB"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="modelo.Producto"%>
<%@page import="modeloDAO.ProductoDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>ComprasWeb</title>
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
        <!-- Styles CSS -->
        <link rel="stylesheet" href="vistas/add.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <header class="header">
                <nav class="nav-main">
                    <div class="logo">
                        <a href="Controlador?accion=listar">ComprasWeb</a>
                    </div>
                    <div class="search">
                        <input type="text" class="search-bar" placeholder="Buscar Productos...">
                        <a href="#"><i class="fas fa-search"></i></a>
                    </div>
                    <div class="btn-menu">
                        <a href="#"><i class="fas fa-bars"></i></a>
                    </div>
                    <ul class="menu">
                        <li><a href="#">Ingresar</a></li>
                        <li><a href="#">Registraté</a></li>
                        <li><a href="#">Vender</a></li>
                        <li><a href="#">Categorias</a></li>
                    </ul>
                </nav>
                <div class="text-header">
                    <h2>Tenemos lo que estas buscando!</h2>
                    <a href="#">Registraté <i class="fas fa-angle-double-right"></i></a>
                </div>
            </header>
            <main class="main">
                <form action="Controlador">
                    <table class="formulario">
                        <%
                            DB cnx = new DB();
                            int id = cnx.generarId();
                        %>
                        <tr>
                            <td><input type="hidden" name="txtId" value="<%=id%>"></td>
                        </tr>
                        <tr>
                            <td>Nombre</td><td><input type="text" name="txtNombre"></td>
                        </tr>
                        <tr>
                            <td>Descripcion</td><td><input type="text" name="txtDescripcion"></td>
                        </tr>
                        <tr>
                            <td>Precio</td><td><input type="text" name="txtPrecio"></td>
                        </tr>
                        <tr>
                            <td>Categoria</td>
                            <td>
                                <select name="selCategoria">
                                    <option value="1">TVs</option>
                                    <option value="2">Monitores</option>
                                    <option value="3">Notebooks</option>
                                    <option value="4">Consolas</option>
                                    <option value="5">Muebles</option>
                                    <option value="6">Electrodomésticos</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Imagen(URL)</td><td><input type="text" name="txtImagen"></td>
                        </tr>
                        <tr>
                            <th colspan="2"><input type="submit" value="Publicar" name="accion"></th>
                        </tr>
                    </table>
                </form>
            </main>
            <footer class="footer">
                <p>Copyright © 2020 ComprasWeb</p>
            </footer>
        </div>

        <script src="https://kit.fontawesome.com/a0c20fd8da.js" crossorigin="anonymous"></script>
    </body>
</html>