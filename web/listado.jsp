<%@page import="java.util.Iterator"%>
<%@page import="modelo.Producto"%>
<%@page import="java.util.List"%>
<%@page import="modeloDAO.ProductoDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>ComprasWeb</title>
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
        <!-- Styles CSS -->
        <link rel="stylesheet" href="vistas/main.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <header class="header">
                <nav class="nav-main">
                    <div class="logo">
                        <a href="#">ComprasWeb</a>
                    </div>
                    <div class="search">
                        <input type="text" class="search-bar" placeholder="Buscar Productos...">
                        <a href="#"><i class="fas fa-search"></i></a>
                    </div>
                    <div class="btn-menu">
                        <a href="#"><i class="fas fa-bars"></i></a>
                    </div>
                    <ul class="menu">
                        <li><a href="#">Ingresar</a></li>
                        <li><a href="#">Registraté</a></li>
                        <li><a href="Controlador?accion=add">Vender</a></li>
                        <li><a href="#">Categorias</a></li>
                    </ul>
                </nav>
                <div class="text-header">
                    <h2>Tenemos lo que estas buscando!</h2>
                    <a href="#">Registraté <i class="fas fa-angle-double-right"></i></a>
                </div>
            </header>
            <main class="main">
                <%
                    ProductoDAO dao = new ProductoDAO();
                    List list = dao.listar();
                    Iterator<Producto> iter = list.iterator();

                    while (iter.hasNext()) {
                        Producto prod = iter.next();
                %>
                <article class="producto">
                    <img src="<%=prod.getImagen()%>">
                    <h2><%=prod.getNombre()%></h2>
                    <p>$<%=prod.getPrecio()%></p>
                    <a href="#" class="btn-comprar">Comprar</a>
                </article>
                <%
                    }
                %>
            </main>
            <footer class="footer">
                <p>Copyright © 2020 ComprasWeb</p>
            </footer>
        </div>

        <script src="https://kit.fontawesome.com/a0c20fd8da.js" crossorigin="anonymous"></script>
    </body>
</html>